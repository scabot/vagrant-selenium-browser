#!/usr/bin/env bash

# set -x # Debug
set -e # Abort script on error
trap "echo '>>>'; echo '>>> ---=== FAILURE ===---'; echo '>>>'" ERR

# TIP: To get a quick look on currently installed version
# > dpkg -l vagrant

#------------------------------------------------------------------------------
# STEP 0 : Provide URL of a .deb archive with a recrent `vagrant`.
#------------------------------------------------------------------------------
URL="https://dl.bintray.com/mitchellh/vagrant/vagrant_1.7.2_x86_64.deb" # URL from http://www.vagrantup.com/downloads.html
DEB="$(basename $URL)"                                      # Match `vagrant_1.7.2_x86_64.deb` part
DEB_VERSION="$(echo $DEB|egrep -o '[0-9]+.[0-9]+.[0-9]+')"  # Match `1.7.2` part

#------------------------------------------------------------------------------
# STEP 1 : Identify actual `vagrant` version
#------------------------------------------------------------------------------
if which vagrant > /dev/null; then
  VERSION="$(vagrant -v|awk '{print $2}')"
else
  VERSION=""
fi

#------------------------------------------------------------------------------
# STEP 2 : Install a more recent `vagrant`
#------------------------------------------------------------------------------
if dpkg --compare-versions "$VERSION" lt "$DEB_VERSION"; then
  echo "Installing vagrant version ${DEB_VERSION}..."
  curl -L -O $URL
  sudo dpkg -i $DEB
  if [ -z "$VERSION" ]; then
    echo "New Vagrant installed. $(vagrant -v)"
  else
    echo "Upgraded from $VERSION to $(vagrant -v)"
  fi
  rm $DEB # Cleanup
else
  echo "Nothing to do."
  echo "$(vagrant -v) is already a recent version."
fi 
