#!/usr/bin/env bash

# WARNING: Not really a script, just a place to store the command.
#          Useful if debugging the behaviour inside the VM

# Already done by nginx, but this is a one liner doing the same:
# Mapping localhost:8000 to remote host port 8000
# > ssh seb@192.168.33.1 -L 8000:localhost:8000 &

# Start Chrome driver
./chromedriver &

HEADLESS=true
if [ $HEADLESS ]; then
  # Headless
  export DISPLAY=:99
  Xvfb $DISPLAY &
else
  # In X11
  startx &
fi

# Start Selenium
java -jar selenium-server-standalone.jar &
