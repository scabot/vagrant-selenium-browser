
--------------------------------------------------------------------------------

                              *** Description ***

  This VM run Ubuntu/Trusty32 with a setup to run Selenium in a headless X11
  environment with Xvfb. Two web browser are installed: Firefox and Chrome.

--------------------------------------------------------------------------------

Quick start.

  1. Upgrade/Install vagrant. Use following script:
     > ./upgrade-vagrant.sh

  2. Start/Provision the VM with vagrant with following command:
     > vagrant up

  3. You can now use the VM with 'Nightwatch'. The parameters are:
     - Selenium location: 192.168.33.33:4444
     - Tatooine location: 192.168.33.1:8000

--------------------------------------------------------------------------------

Warning:
  The `Vagrant` in Ubuntu 14.04(trusty) is an old 1.4.3 and will fail to downlaod the box.
  To fix this, use the latest vagrant.
  Execute the provided script `upgrade-vagrant.sh` to install a more recent version.

Note:
  To avoid downloading the box over and over again, use this command:
  > vagrant box add ubuntu/trusty32

Definition:
  Host  <=> OS hosting the VM
  Guess <=> OS inside the VM

Notice: X11-forwarding is enable, so if the Guest run an X app (ex. firefox)
        then the window will show up on the host X11 display.
        So, no need to have X11 installed on the guess, only the host.

--------------------------------------------------------------------------------
Structure:

  Host:
    run (grunt dev) Tatouine on port 8000
    run nightwatch

  Guest:
    run (deamon)  Proxy(nginx) (guest|localhost:8000 <=> host|localhost:8000)
    run (crontab) Selenium on port 4444 (feeded by Nightwatch)
    run (crontab) ChromeDriver (Use by Selenium)

Parameters:
  Guest IP
  Host IP
  Selenium Port
  Tatouine Port
  Proxy `hostname:port` for Tatouine [host/localhost:8000 <=> guest/localhost:8000]

Components:
  - nginx Proxy
  - Vagrantfile Selenium port forwarding 4444
  - Chrome WebDriver
  - Selenium
  - Xvfb

--------------------------------------------------------------------------------
Fixme: Keep a copy of 'Selenium', 'Chrome WebDriver' and 'Vagrant' on S3.
       Otherwise, the day one of these web site are not available we can not work.
--------------------------------------------------------------------------------
