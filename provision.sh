#!/usr/bin/env bash

set -e # Abort script on error
trap "echo '>>>'; echo '>>> ---=== FAILURE ===---'; echo '>>>'" ERR

#set -x # Debug

# ------------------------------------------------------------------------------
function log {
  echo "     ---------------------------------------------------"
  echo "     -----   $1 "
  echo "     ---------------------------------------------------"
}

# ------------------------------------------------------------------------------
# Google Chrome (PPA)
# ------------------------------------------------------------------------------
log 'Setup Chrome PPA'
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - 
if [ ! -f /etc/apt/sources.list.d/google-chrome.list ]; then
  echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list
fi

# ------------------------------------------------------------------------------
# Update/Upgrade/Install Ubuntu 14.04(trusty) packages
# ------------------------------------------------------------------------------
log 'The `apt-get update`' && apt-get update

log 'The `apt-get upgrade' && apt-get -y upgrade # { dnl: '640 K', disk: '4 K' }

log 'Install unzip'   && apt-get -y install unzip                # Use in this script   {     zip: { dnl: '150 K,  disk: '338 K' }
log 'Install xvfb'    && apt-get -y install xvfb                 # Headless X-Windows   {    xvfb: { dnl: '740 K', disk:   '2 M' }
log 'Install firefox' && apt-get -y install firefox              # Web Browser: Firefox { firefox: { dnl:  '50 M,  disk: '134 M' }
log 'Install chrome'  && apt-get -y install google-chrome-stable # Web Browser: Chrome  {  chrome: { dnl:  '55 M', disk: '222 M' }
log 'Install jre'     && apt-get -y install default-jre-headless # To run Selenium      {     jre: { dnl:  '42 M', disk:  '68 M' )
log 'Install nginx'   && apt-get -y install nginx                # Our Proxy            {   nginx: { dnl: '400 K', disk: '1.8 M' }


# For DEBUG only (Lauch X11 with `startx`, then use command in `debug.sh` located in /vagrant share folder)
# apt-get install -y xorg  # X11 server     {   x11: { dnl: '2 M', disk: '7 M' }
# apt-get install -y icewm # Window manager { icewm: { dnl: '1 M', disk: '6 M' }

# ------------------------------------------------------------------------------
log 'Setup nginx (proxy)'
# ------------------------------------------------------------------------------
# Tatooine IP:PORT (Assumption here: The VM Host run Tatooine)
#                  (Do this mapping: Ex. 192.168.33.33 => 192.168.33.1)
IP="$(ifconfig eth1 | grep "inet addr" | awk -F: '{print $2}' | awk '{print $1}' | awk -F. '{print $1"."$2"."$3".1"}')"
PORT="8000"
cat - > /etc/nginx/sites-available/default <<-EOF
	server {
	  listen       8000;      # The only working port     (required by backend)
	  server_name  localhost; # The only working hostname (required by backend)
	  location ~ ^/.*$ {
	    proxy_pass  http://${IP}:${PORT}; # Tatouine IP:PORT
	  }
	}
EOF
service nginx restart

# ------------------------------------------------------------------------------
log 'Get Selenium'
# ------------------------------------------------------------------------------
JAR="selenium-server-standalone.jar"
if [ ! -f "$JAR" ]; then
  echo "Downloading Selenium..."
  # NOTE: Use latest at `http://selenium-release.storage.googleapis.com/index.html`
  curl -s http://selenium-release.storage.googleapis.com/2.45/selenium-server-standalone-2.45.0.jar > $JAR
fi
chown vagrant:vagrant $JAR

# ------------------------------------------------------------------------------
log 'Get Chrome WebDriver'
# ------------------------------------------------------------------------------
ZIP_FILE="chromedriver_linux32.zip"
if [ ! -f "$ZIP_FILE" ]; then
  LATEST="$(curl -s http://chromedriver.storage.googleapis.com/LATEST_RELEASE)"
  echo "Downloading Chrome Driver"
  curl -s -O http://chromedriver.storage.googleapis.com/$LATEST/$ZIP_FILE
fi

DRIVER_FILE="chromedriver"
if [ ! -f "$DRIVER_FILE" ]; then
  unzip "$ZIP_FILE"
fi
chown vagrant:vagrant "$DRIVER_FILE"

# ------------------------------------------------------------------------------
log 'Set `crontab` to Lauch `Xvfb/Selenium` and `Chrome WebDriver` at boot time'
# ------------------------------------------------------------------------------
# NOTE: Since X11-forward is On, we can rely on host X11 to open browser window.
#       With this big assumption, we do not need xvfb anymore.
#       Then, it's enough to just use this crontab command:
#       > @reboot java -jar selenium-server-standalone.jar
crontab -u vagrant - <<-EOF
	@reboot xvfb-run -e /home/vagrant/xvfb.err -s "-ac -screen 0 1024x768x8" java -jar /home/vagrant/selenium-server-standalone.jar
	@reboot /home/vagrant/chromedriver
EOF

# ------------------------------------------------------------------------------
log 'Reboot the VM'
# ------------------------------------------------------------------------------
reboot
